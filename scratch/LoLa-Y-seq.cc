/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2018,
 *     Karlsruhe Institute of Technology
 *     Institute of Telematics
 *     Zirkel 2, 76131 Karlsruhe
 *     Germany
 *
 *     Author: Felix Neumeister <felix.neumeister@student.kit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * This program is in parts based on and inspired by the example files shipped with ns-3.
 */

#include <string>
#include <fstream>
#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include "ns3/network-module.h"
#include "ns3/packet-sink.h"
#include "ns3/netanim-module.h"
#include "ns3/point-to-point-layout-module.h"
#include "ns3/gnuplot-helper.h"
#include "ns3/file-helper.h"
#include "ns3/random-variable-stream.h"
#include "ns3/pointer.h"
//#include "ns3/"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("TcpLoLaBulkSendExample");


int
main (int argc, char *argv[])
{

	bool tracing = false;
	uint32_t maxBytes = 0;
	uint32_t duration = 33;
	std::string datarate = "1000Mbps";
	bool with_err = false;
	std::string delay = "10ms";
	bool jitter = false;
	bool pacing = true;
	int lola_filter = 2;
	uint32_t shortflows = 0;
	uint32_t cb_version = 2;


	uint32_t queueSize = 100;
	//uint32_t tcpBufSize = 80000000; //100ms bei 1G
	//uint32_t tcpBufSize = 320000000; //40ms bei 10G
	//uint32_t tcpBufSize = 640000000; //80ms bei 10G

	uint32_t writeSize = 1500;
	uint32_t gqueueSize = 100000000;
	int flows = 2;
	int flows2 = 2;
	bool logSocketBase = false;
	bool fakeCubic = false;
	//
	// Allow the user to override any of the defaults at
	// run-time, via command-line arguments
	//
	CommandLine cmd;
	cmd.AddValue ("tracing", "Flag to enable/disable tracing", tracing);
	cmd.AddValue ("maxBytes",
			"Total number of bytes for application to send", maxBytes);
	cmd.AddValue("duration", "Sim Time", duration);
	cmd.AddValue("datarate", "Datarate of bottleneck", datarate);
	cmd.AddValue("witherr", "Introduce errors on link", with_err);
	cmd.AddValue("delay", "bottleneck link delay", delay);
	cmd.AddValue("flows1","# of flows at 0",flows);
	cmd.AddValue("flows2", "# of flows at 5",flows2);
	cmd.AddValue("cb_version", "CB Version: none (0), conservative (1), quick (2)", cb_version);

	uint32_t tcpBufSize = 320000000 / flows ; //80ms bei 10G


	cmd.AddValue("tcpBufSize","Setting the tcp socket buff size",tcpBufSize);
	cmd.AddValue("queueSize","Queue size in ms for bottleneck router, support range > 0",queueSize);
	cmd.AddValue("writeSize","Setting the Maximum Segment Size(MSS) for TCP",writeSize);
	cmd.AddValue("jitter","Whether or not to introduce 10% bw jitter",jitter);
	cmd.AddValue("logSocketBase", "debug on Socket Base", logSocketBase);
	cmd.AddValue("pacing", "enable or disable pacing", pacing);
	cmd.AddValue("filter", "which rtt filter should be used (1 simpleMin, 2 MaxofMin)", lola_filter);
	cmd.AddValue("shortflows", "Size of the short flows in Bytes",shortflows);
	cmd.AddValue("fakeCubic", "if true, cbstart & target are set to 1s", fakeCubic);


	cmd.Parse (argc, argv);

	//
	// Explicitly create the nodes required by the topology (shown above).
	//

	//Set Segment Size for TCP
	Config::SetDefault("ns3::TcpSocket::SegmentSize",UintegerValue(writeSize));

	//Set up the TCP receive and send buffer size
	Config::SetDefault("ns3::TcpSocket::RcvBufSize",UintegerValue(tcpBufSize));
	Config::SetDefault("ns3::TcpSocket::SndBufSize",UintegerValue(tcpBufSize));
	Config::SetDefault("ns3::TcpSocketBase::Pacing", BooleanValue(pacing));






	//Set Queue for Net Devices
	Config::SetDefault("ns3::QueueBase::MaxPackets",UintegerValue(gqueueSize));




	/*//------------Link Configuration------------------------

  	Ptr<RateErrorModel> em = CreateObject<RateErrorModel> ();
    em->SetAttribute ("ErrorRate", DoubleValue (0.00001));
    devices.Get (1)->SetAttribute ("ReceiveErrorModel", PointerValue (em));


  	PointToPointHelper config1;
  	config1.SetDeviceAttribute  ("DataRate", StringValue ("100000Mbps"));
  	config1.SetChannelAttribute ("Delay", StringValue ("0.5ms"));
  	config1.SetQueue("ns3::DropTailQueue", "MaxPackets", UintegerValue(bqueueSize));

  	 //------------------------------------------------
	 */


	//--------------CreateNodes---------------------------------------




	// Create the point-to-point link helpers
	PointToPointHelper pointToPointL;
	pointToPointL.SetDeviceAttribute  ("DataRate", StringValue ("100000Mbps"));
	pointToPointL.SetChannelAttribute ("Delay", StringValue ("0.5ms"));
	PointToPointHelper pointToPointR;
	pointToPointR.SetDeviceAttribute    ("DataRate", StringValue ("100000Mbps"));
	pointToPointR.SetChannelAttribute   ("Delay", StringValue ("0.5ms"));
	PointToPointHelper pointToPointRouter;
	pointToPointRouter.SetDeviceAttribute    ("DataRate", StringValue (datarate));
	pointToPointRouter.SetChannelAttribute   ("Delay", TimeValue (((Time(delay) - Time("2ms"))/2)));
	pointToPointRouter.SetQueue("ns3::DropTailQueue", "MaxBytes", UintegerValue(queueSize*DataRate(datarate).GetBitRate()/8/1000));



	PointToPointDumbbellHelper d (3, pointToPointL,
			3, pointToPointR,
			pointToPointRouter);


	// Install Stack
	InternetStackHelper stack;
	d.InstallStack (stack);

	// Assign IP Addresses
	d.AssignIpv4Addresses (Ipv4AddressHelper ("10.1.1.0", "255.255.255.0"),
			Ipv4AddressHelper ("10.2.1.0", "255.255.255.0"),
			Ipv4AddressHelper ("10.3.1.0", "255.255.255.0"));

	//
	Ipv4GlobalRoutingHelper::PopulateRoutingTables ();



	NS_LOG_INFO ("Create Applications.");



	uint16_t port = 9;  // well-known echo port number






	//---------Config lola Parameters-------------


	ObjectFactory cc_fact;// = Create<ObjectFactory>();
	cc_fact.SetTypeId(TypeId::LookupByName("ns3::TcpLoLa"));
	cc_fact.Set("lola_gradient_version", UintegerValue(0));
	cc_fact.Set("lola_rtt_filter", UintegerValue(lola_filter));
	//cc_fact.Set("lola_fixed_base_rtt", TimeValue(Time(delay)));
	cc_fact.Set("lola_CB_version", UintegerValue(cb_version));
	if (fakeCubic){
		cc_fact.Set("lola_cb_start", TimeValue(Time("1s")));
		cc_fact.Set("lola_target", TimeValue(Time("1s")));
	}


	//cc_fact->Set("");


	/*
	 * configuration for all:
	 * gradient version = 0
	 *
	 * CB_conservative:
	 * set CB_version = 1
	 *
	 * (CB naive:
	 * set CB_version = 0)
	 *
	 * CB_quick
	 * set CB_version = 2
	 */
	//--------------------------------------------------------------



	InetSocketAddress target =  InetSocketAddress (d.GetRightIpv4Address(0), 5001);
	uint32_t initialCwnd = 5;
	uint32_t flow_size = 0;



	//-------------Config Flexible Sender-----------------------------

	BulkSendHelper source ("ns3::TcpSocketFactory",target);
	source.SetAttribute("CC", ObjectFactoryValue(cc_fact));
	source.SetAttribute("InitialWindow", UintegerValue(initialCwnd));
	source.SetAttribute("InitialSSThresh", UintegerValue(1));
	source.SetAttribute ("MaxBytes", UintegerValue (flow_size));

	//----------------Config Receiver----------------------------------

	PacketSinkHelper sink ("ns3::TcpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), 5001));


	//--------------------------------------------------------------



	ApplicationContainer sinkApps;
	ApplicationContainer sourceApps1;

	for(int x = 0; x < flows; x++){
		source.SetAttribute("Remote", AddressValue(InetSocketAddress (d.GetRightIpv4Address(0), port)));
		sourceApps1.Add(source.Install(d.GetLeft (port%2)));

		sink.SetAttribute("Local", AddressValue(InetSocketAddress (Ipv4Address::GetAny (), port)));
		sink.Install(d.GetRight(0));

		port++;
	}
	sourceApps1.Start(Seconds(0.001));
	sourceApps1.Stop(Seconds(200));

	ApplicationContainer sourceApps2;

	for(int x = 0; x < flows2; x++){
		source.SetAttribute("Remote", AddressValue(InetSocketAddress (d.GetRightIpv4Address(0), port)));
		source.SetAttribute("InitialWindow", UintegerValue(DataRate(datarate).GetBitRate() / 8 * Time(delay).GetMicroSeconds()/(writeSize + 16 +16 +10) /Time("1s").GetMicroSeconds() / flows2));
		sourceApps2.Add(source.Install(d.GetLeft (port%2)));

		sink.SetAttribute("Local", AddressValue(InetSocketAddress (Ipv4Address::GetAny (), port)));
		sink.Install(d.GetRight(0));

		port++;
	}
	sourceApps2.Start(Seconds(0.01));
	sourceApps2.Stop(Seconds(200));


	if (shortflows){
		Time current = Time("5s");

		Ptr<UniformRandomVariable>startgap = CreateObject<UniformRandomVariable> ();
		startgap->SetAttribute ("Min", DoubleValue (200));
		startgap->SetAttribute ("Max", DoubleValue (1200));

		source.SetAttribute("InitialSSThresh", UintegerValue(65000000));
		source.SetAttribute("InitialWindow", UintegerValue(1));

		while (current < Seconds(duration)){
			//for (int x = 0; x < shortflows; x++){

			source.SetAttribute("Remote", AddressValue(InetSocketAddress (d.GetRightIpv4Address(0), port)));
			source.SetAttribute ("MaxBytes", UintegerValue (shortflows));

			ApplicationContainer sender;
			sender.Add(source.Install(d.GetLeft (port%2)));
			sender.Start(current);
			sender.Stop(Seconds(200.0));

			sink.SetAttribute("Local", AddressValue(InetSocketAddress (Ipv4Address::GetAny (), port)));
			sink.Install(d.GetRight(0));

			port++;
			current += MilliSeconds(startgap->GetValue());

		}


	}



	if (jitter){
		// Install on/off app on all right side nodes
		// Constant gives time in seconds
		OnOffHelper clientHelper ("ns3::UdpSocketFactory", Address ());
		clientHelper.SetAttribute ("OnTime", StringValue ("ns3::UniformRandomVariable[Min=0.001|Max=0.005]"));
		clientHelper.SetAttribute ("OffTime", StringValue ("ns3::UniformRandomVariable[Min=0.05|Max=0.5]"));
		clientHelper.SetAttribute ("DataRate", DataRateValue((DataRate(.2 * DataRate(datarate).GetBitRate()))));
		Address sinkLocalAddress (InetSocketAddress (Ipv4Address::GetAny (), port));
		PacketSinkHelper packetSinkHelper ("ns3::UdpSocketFactory", sinkLocalAddress);
		sinkApps.Add (packetSinkHelper.Install (d.GetRight (2)));

		ApplicationContainer clientApps;
		AddressValue remoteAddress (InetSocketAddress (d.GetRightIpv4Address (2), port));
		clientHelper.SetAttribute ("Remote", remoteAddress);
		clientApps.Add (clientHelper.Install (d.GetLeft (2)));

		clientApps.Start (Seconds (1.0)); // Start 1 second after sink
		clientApps.Stop (Seconds (duration)); // Stop before the sink
		port++;

	}

	sinkApps.Start (Seconds (0.0));
	sinkApps.Stop (Seconds (duration));


	//set Logging
	LogComponentEnable ("TcpLoLa", LOG_LEVEL_INFO);
	if (logSocketBase) LogComponentEnable ("TcpSocketBase", LOG_LEVEL_DEBUG);

	NS_LOG_INFO ("Run Simulation.");
	Simulator::Stop (Seconds (30.00));
	Simulator::Run ();
	Simulator::Destroy ();
	NS_LOG_INFO ("Done.");

	Ptr<PacketSink> sink1 = DynamicCast<PacketSink> (sinkApps.Get (0));
	std::cout << "Total Bytes Received: " << sink1->GetTotalRx () << std::endl;
}
