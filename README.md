This repository contains the ns-3 simulation code for TCP-Lola

The master branch contains an implementation of the original LoLa algorithm.
The FFB\_quick branch in addition contains an implementation for FFB\_quick.
The original\_files branch contains all original files from ns-3.27, which have been altered.

The main LoLa code can be found in /src/internet/model.
In /scratch, one can also find a dummy simulation scenario for preliminary tests.

The other files in this repository are necessary to be able to run successful tests with ns-3.27. 
Among other things, they address:
- Pacing
- TCP-timestamps
- RTT-accounting
- Queue-lengths
