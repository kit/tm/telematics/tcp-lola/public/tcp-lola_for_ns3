/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2018,
 *     Karlsruhe Institute of Technology
 *     Institute of Telematics
 *     Zirkel 2, 76131 Karlsruhe
 *     Germany
 *
 *     Author: Felix Neumeister <felix.neumeister@student.kit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "tcp-lola.h"
#include "ns3/tcp-socket-base.h"
#include "ns3/log.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("TcpLoLa");
NS_OBJECT_ENSURE_REGISTERED (TcpLoLa);


TypeId
RTTAccounting::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::RTTAccounting")
    //.SetParent<TcpLoLa> ()
    //.AddConstructor<RTTAccounting> ()
    .SetGroupName ("Internet")
	//TODO: List all atributes...
    /*.AddAttribute ("Queue Target", "Queue Target",
                   UintegerValue (2),
                   MakeUintegerAccessor (&TcpLoLa::lola_queue_target),
                   MakeUintegerChecker<Time> ())
    .AddAttribute ("CWnd min", "Minimal CWnd to reduce to",
                   UintegerValue (4),
                   MakeUintegerAccessor (&TcpLoLa::lola_CWnd_min),
                   MakeUintegerChecker<uint32_t> ())
    .AddAttribute ("CB Start Delay", "Convergence Booster start delay",
                   UintegerValue (1),
                   MakeUintegerAccessor (&TcpLoLa::lola_convergence_booster_start_delay),
                   MakeUintegerChecker<Time> ())
   .AddAttribute ("CB Sigma", "Convergence Booster sigma",
					UintegerValue (1),
					MakeUintegerAccessor (&TcpLoLa::lola_convergence_booster_sigma),
					MakeUintegerChecker<uint32_t> ())*/
  ;
  return tid;
}




SimpleMin::SimpleMin(Time m_time){
	measurement_time = m_time;
	measurement_end = Simulator::Now() + measurement_time;
	measurement_count = 0;
	rtt_min = Time::Max();
	rtt_base = Time::Max();
	locked_until = Time::Min();
	in_hold = false;
	fixed_base == Time::Min();

}
void SimpleMin::set_hold(bool hold){
	in_hold = hold;
}

void SimpleMin::set_fixed_base(Time base){
	fixed_base = base;
}

SimpleMin::SimpleMin(){
	measurement_time = Time("60ms");
	measurement_end = Simulator::Now() + measurement_time;
	measurement_count = 0;
	rtt_min = Time::Max();
	rtt_base = Time::Max();
	locked_until = Time::Min();
	in_hold = false;
}

void SimpleMin::update_RTT(Ptr<TcpSocketState> tcb, uint32_t segmentsAcked,
		const Time& rtt){
	//if(locked_until < Simulator::Now()){

	if (!in_hold){
		measurement_count = measurement_count + segmentsAcked;
		if(rtt_min > rtt){
			rtt_min = rtt;
		}
	}
	if(rtt_base > rtt){
		rtt_base = rtt;
	}

}


void MaxofMin::set_hold(bool hold){
	in_hold = hold;
	if (!hold) holdMax = Time::Min();
}



MaxofMin::MaxofMin(Time time){
	SimpleMin();
	holdMax = Time::Min();
}
MaxofMin::MaxofMin(){
	SimpleMin();
	holdMax = Time::Min();
}

bool SimpleMin::is_ready(Ptr<TcpSocketState> tcb){
	NS_LOG_FUNCTION(this );
	NS_LOG_DEBUG (this <<"end="<< measurement_end.GetMicroSeconds() << " now="<<Simulator::Now().GetMicroSeconds());
	return ((measurement_end < Simulator::Now()) && (measurement_count >= 1));
}

Time SimpleMin::get_current_RTT() {
	return rtt_min;
}

Time SimpleMin::get_base(){
	return  rtt_base;
}

void SimpleMin::lock(Time lock){
	locked_until = Simulator::Now() + lock;
	measurement_end = Simulator::Now() + lock + measurement_time;
}

void SimpleMin::reset_all(){
	rtt_min = Time::Max();
	rtt_base = Time::Max();
	measurement_count = 0;
	measurement_end = Simulator::Now() + measurement_time;
}

void SimpleMin::next_measurement(){
	NS_LOG_FUNCTION(this);
	NS_LOG_DEBUG ("mend " << measurement_end);
	if (measurement_end < Simulator::Now())
	{
		NS_LOG_DEBUG ("Resetting Measurement");
		rtt_min = Time::Max();
		measurement_count = 0;
		measurement_end = Simulator::Now() + measurement_time;

	}else
		NS_LOG_DEBUG ("Not resetting - measurement ended");
}


bool SSMin::is_ready(Ptr<TcpSocketState> tcb){
	NS_LOG_FUNCTION(this);
	NS_LOG_DEBUG ((tcb != NULL));
	NS_LOG_DEBUG ("mcount " << measurement_count << ", cwnd: "<< tcb->GetCwndInSegments());
	return ( (measurement_count >= (tcb->GetCwndInSegments())/4) && measurement_count >= 4);
}

void SSMin::next_measurement(){
	NS_LOG_FUNCTION(this);
	NS_LOG_DEBUG ("mend " << measurement_end << "rtt base" << rtt_base);
	measurement_count = 0;
	rtt_min = Time::Max();

}

void MaxofMin::update_RTT(Ptr<TcpSocketState> tcb, uint32_t segmentsAcked,
		const Time& rtt){
	if (in_hold)
		if (SimpleMin::is_ready(tcb)){
			holdMax = (holdMax < SimpleMin::get_current_RTT() ? SimpleMin::get_current_RTT() : holdMax);
			next_measurement();
		}
	measurement_count = measurement_count + segmentsAcked;
	if(rtt_min > rtt){
		rtt_min = rtt;
	}
	if(rtt_base > rtt){
		rtt_base = rtt;
	}

}

Time MaxofMin::get_current_RTT() {
	if (holdMax > Time::Min())
		return holdMax;
	else
		return rtt_min;
}

bool MaxofMin::is_ready(Ptr<TcpSocketState> tcb) {
	return SimpleMin::is_ready(tcb) || holdMax != Time::Min();
}



TypeId
TcpLoLa::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::TcpLoLa")
    .SetParent<TcpNewReno> ()
    .AddConstructor<TcpLoLa> ()
    .SetGroupName ("Internet")
	//TODO: List all atributes...
    .AddAttribute ("lola_target", "Queue Target",
                   TimeValue (Time("5ms")),
                   MakeTimeAccessor (&TcpLoLa::lola_queue_target),
                   MakeTimeChecker())
    .AddAttribute ("CWnd_min", "Minimal CWnd to reduce to",
                   UintegerValue (2),
                   MakeUintegerAccessor (&TcpLoLa::lola_CWnd_min),
                   MakeUintegerChecker<uint32_t> ())
   .AddAttribute ("CB_Sigma", "Convergence Booster sigma",
					UintegerValue (35),
					MakeUintegerAccessor (&TcpLoLa::lola_convergence_booster_sigma),
					MakeUintegerChecker<uint32_t> ())
   .AddAttribute ("lola_gamma", "Overreduction",
					UintegerValue (975),
					MakeUintegerAccessor (&TcpLoLa::lola_gamma),
					MakeUintegerChecker<uint32_t> ())
   .AddAttribute ("lola_rtt_filter", "Which RTT-filter should be used",
					UintegerValue (2),
					MakeUintegerAccessor (&TcpLoLa::lola_rtt_filter),
					MakeUintegerChecker<uint32_t> ())
   .AddAttribute ("lola_gradient_version", "Rtt gradient over rtt (1) or Measurement Interval(0)",
					UintegerValue (0),
					MakeUintegerAccessor (&TcpLoLa::cb_gradient_version),
					MakeUintegerChecker<uint32_t> ())
   .AddAttribute ("lola_CB_version", "CB version none(0), normal(1)",
					UintegerValue (2),
					MakeUintegerAccessor (&TcpLoLa::lola_CB_version),
					MakeUintegerChecker<uint32_t> ())
   .AddAttribute ("lola_fixed_base_rtt", "Set a fixed Base delay",
					TimeValue (Time(0)),
					MakeTimeAccessor(&TcpLoLa::lola_fixed_base_delay),
					MakeTimeChecker())
   .AddAttribute ("lola_cb_start", "Set CB start delay",
					TimeValue (Time("1ms")),
					MakeTimeAccessor(&TcpLoLa::lola_convergence_booster_start_delay),
					MakeTimeChecker())
;
  return tid;
}



void TcpLoLa::set_fixed_base_delay(Time base){
	lola_rtts->set_fixed_base(base);
	ss_rtts->set_fixed_base(base);
}

TcpLoLa::TcpLoLa(void)
: TcpNewReno (),
  beta (717),
  initial_ssthresh (2),
  bic_scale (41),
  scale (4),


  lola_queue_target (Time("5ms")),
  lola_gamma (950),
  lola_CWnd_min (2),
  lola_convergence_booster_start_delay (Time("1ms")),
  lola_convergence_booster_sigma (35),
  lola_CB_version(2),
  lola_hold_time (Time("250ms")),
  lola_min_target_recalc_interval(Time("100us")),


  //LoLa mode flags
  lola_do_precautionary_decongestion (true),
  lola_do_convergence_booster (true),
  lola_do_CWnd_hold (true),
  lola_gradient (0),

  // Connection specific variables
  // CUBIC Curve
  cubic_epoch_start (Time::Min()),
  cubic_origin_point (0),
  cubic_k (0),
  cubic_last_max (0),
  cnt (0),


  // Convergence Booster state
  cb_epoch_start (Time::Min()),
  cb_gradient_version(0),

  // Loss Recovery
  lr_loss_cwnd (0),


  // LoLa state
  in_hold (false),
  in_convergence_booster (false),
  in_cubic (false),

  lola_rtt_filter(2),
  lola_fixed_base_delay(Time(0)),
  lola_rtts ((lola_rtt_filter == 2) ? new MaxofMin() : new SimpleMin()),
  ss_rtts (new SSMin()),
  lola_curr_rtts (ss_rtts)

{
	NS_LOG_FUNCTION (std::to_string(id) << ">>>" <<this);
	id = rand();
	std::cout << "Lola initiated";
	// Base RTT Measurement
	/*
	 * // LoLa Rtt measuremt variables
	 lola_measurement_time (Time::Time("10ms")),
	 lola_base_timeout (10),
	 lola_base_epsilon (Time::Time("100us")),
	 lola_min_samples (10),
  	 rtt_base (Time::Max()),
  	 rtt_sample_count (0),
  	 rtt_base_invalidation_count (0),

  	 // RTT Measurement (Min)
  	 rtt_curr (Time::Max()),
  	 rtt_end_measurement (Time::Time("0s")),

  	 // RTT Measurement (Filter)
  	 Time *rtt_buffer;
  	 uint32_t rtt_buffersize;
  	 Time *rtt_current;*/
}

TcpLoLa::TcpLoLa (const TcpLoLa& sock)
: TcpNewReno (sock),
  beta (717),
  initial_ssthresh (sock.initial_ssthresh),
  bic_scale (sock.bic_scale),
  scale (sock.scale),


  lola_queue_target (sock.lola_queue_target),
  lola_gamma (sock.lola_gamma),
  lola_CWnd_min (sock.lola_CWnd_min),
  lola_convergence_booster_start_delay (sock.lola_convergence_booster_start_delay),
  lola_convergence_booster_sigma (sock.lola_convergence_booster_sigma),
  lola_CB_version(sock.lola_CB_version),
  lola_hold_time (sock.lola_hold_time),



  //LoLa mode flags
  lola_do_precautionary_decongestion (sock.lola_do_precautionary_decongestion),
  lola_do_convergence_booster (sock.lola_do_convergence_booster),
  lola_do_CWnd_hold (sock.lola_do_CWnd_hold),
  lola_gradient (sock.lola_gradient),

  // Connection specific variables
  // CUBIC Curve
  cubic_epoch_start (sock.cubic_epoch_start),
  cubic_origin_point (sock.cubic_origin_point),
  cubic_k (sock.cubic_k),
  cubic_last_max (sock.cubic_last_max),
  cnt (sock.cnt),


  // Convergence Booster state
  cb_epoch_start (sock.cb_epoch_start),
  cb_gradient_version(sock.cb_gradient_version),


  // Loss Recovery
  lr_loss_cwnd (sock.lr_loss_cwnd),


  // LoLa state
  in_hold (sock.in_hold),
  in_convergence_booster (sock.in_convergence_booster),
  in_cubic (sock.in_cubic),

  // RTT measurement
  lola_rtt_filter(sock.lola_rtt_filter), //TODO: fix to set with real accessor
  lola_fixed_base_delay(sock.lola_fixed_base_delay),
  lola_rtts (sock.lola_rtts),
  ss_rtts (sock.ss_rtts),
  lola_curr_rtts (ss_rtts)
{
	NS_LOG_FUNCTION (std::to_string(id) << ">>>" <<this);
}

TcpLoLa::~TcpLoLa (void)
{
	NS_LOG_FUNCTION (std::to_string(id) << ">>>" <<this);
	//delete lola_rtts; //TODO: Memory Leak here - fix for large scale sim
}

Ptr<TcpCongestionOps>
TcpLoLa::Fork (void)
{
	return CopyObject<TcpLoLa> (this);
}

void
TcpLoLa::PktsAcked (Ptr<TcpSocketState> tcb, uint32_t segmentsAcked,
		const Time& rtt)
{
	NS_LOG_FUNCTION (std::to_string(id) << ">>>" <<this << tcb << segmentsAcked << rtt);

	if (rtt.IsZero ())
	{
		NS_LOG_DEBUG (std::to_string(id) + ">>>" +  "Discarded zero RTT measurement");
		return;
	}

	lola_rtts->update_RTT(tcb, segmentsAcked, rtt);
	ss_rtts->update_RTT(tcb, segmentsAcked, rtt);

}

void
TcpLoLa::CongestionStateSet (Ptr<TcpSocketState> tcb,
		const TcpSocketState::TcpCongState_t newState)
{
	NS_LOG_FUNCTION (std::to_string(id) << ">>>" <<this << tcb << newState);
	if (newState == TcpSocketState::CA_OPEN)
	{

		//TODO: Cubic start shift
	}
}

void
TcpLoLa::DoPreDecog(Ptr<TcpSocketState> tcb){
	NS_LOG_FUNCTION (std::to_string(id) << ">>>" <<this);
	if(!lola_curr_rtts->is_ready(tcb))
		return;

	Time queue_delay = lola_curr_rtts->get_current_RTT() - (lola_fixed_base_delay == Time(0) ? lola_curr_rtts->get_base()  : lola_fixed_base_delay);
	NS_LOG_DEBUG (std::to_string(id) << ">>>" <<"rtt ready: curr_rtt: " << lola_curr_rtts->get_current_RTT().GetMicroSeconds()<< " base delay: " << (lola_fixed_base_delay == Time(0) ? lola_curr_rtts->get_base()  : lola_fixed_base_delay).GetMicroSeconds() << " queue_delay: " << queue_delay.GetMicroSeconds());

	if (tcb->m_cWnd < tcb->m_ssThresh){
		//if (queue_delay > lola_convergence_booster_start_delay * 2){
		if (queue_delay > 2 * lola_queue_target){
			tcb->m_ssThresh = tcb->m_cWnd;
			StartCUBIC(tcb);
			lola_rtts->next_measurement();
		}
		lola_curr_rtts->next_measurement();
		return;

	}

	if(lola_do_convergence_booster && ! in_hold && ! in_convergence_booster
			&& queue_delay > lola_convergence_booster_start_delay
			&& (tcb->m_cWnd >= tcb->m_ssThresh) && queue_delay < lola_queue_target){
		StartCB(tcb);
	}
	if(queue_delay > lola_queue_target)
	{
		if (lola_do_CWnd_hold && ! in_hold && tcb->m_cWnd >= tcb->m_ssThresh){
			in_hold = true;
			lola_rtts->set_hold(true);
			in_convergence_booster = in_cubic = false;
			hi_end = Simulator::Now() + lola_hold_time;
			NS_LOG_DEBUG (std::to_string(id) << ">>>" <<"Going into hold until " << hi_end);
			//lola_rtts->next_measurement();
			return;
		}
		in_hold = in_convergence_booster = in_cubic = false;
		cubic_last_max = tcb->GetCwndInSegments();
		NS_LOG_DEBUG (std::to_string(id) << ">>>" <<"cwnd " << tcb->GetCwndInSegments() << " base " << (lola_fixed_base_delay == Time(0) ? lola_curr_rtts->get_base()  : lola_fixed_base_delay).GetMicroSeconds() << " rtt "<< lola_curr_rtts->get_current_RTT().GetMicroSeconds() << " min " << lola_CWnd_min);
		uint64_t cwnd;
		cwnd = ((uint64_t)tcb->GetCwndInSegments()) * (lola_fixed_base_delay == Time(0) ? lola_curr_rtts->get_base()  : lola_fixed_base_delay).GetMicroSeconds() * lola_gamma;
		cwnd = cwnd / (uint64_t)lola_curr_rtts->get_current_RTT().GetMicroSeconds() / 1000;
		cwnd = (uint32_t)std::max((uint64_t)lola_CWnd_min, cwnd);
		NS_LOG_DEBUG (std::to_string(id) << ">>>" <<"new cwnd in segments " << cwnd <<" with cwnd " << tcb->m_cWnd);
		tcb->m_cWnd = cwnd * tcb->m_segmentSize;
		//cWnd change
		tcb->m_ssThresh = cwnd * tcb->m_segmentSize;
		StartCUBIC(tcb);

		lola_rtts->set_hold(false);
		lola_rtts->lock(lola_curr_rtts->get_current_RTT());
		lola_rtts->next_measurement();



	}

}

void
TcpLoLa::StartCB(Ptr<TcpSocketState> tcb){
	NS_LOG_FUNCTION (std::to_string(id) << ">>>" <<this);
	in_cubic = in_hold = false;
	in_convergence_booster = true;
	cb_epoch_start = Simulator::Now();
	cubic_epoch_start = Time::Min();
	cb_Q_data_begin =  tcb->m_cWnd * (lola_curr_rtts->get_current_RTT() - (lola_fixed_base_delay == Time(0) ? lola_curr_rtts->get_base()  : lola_fixed_base_delay)).GetMicroSeconds()/ lola_curr_rtts->get_current_RTT().GetMicroSeconds();
	lola_gradient = 100*tcb->GetCwndInSegments();
	cb_last_change = Time(0);
}

void TcpLoLa::StartCUBIC(Ptr<TcpSocketState> tcb){
	NS_LOG_FUNCTION (std::to_string(id) << ">>>" <<this);
	in_convergence_booster = in_hold = false;
	cb_epoch_start = Time::Min();
	in_cubic = true;

	cubic_epoch_start = Simulator::Now ();   // record the beginning of an epoch
	uint32_t segCwnd = tcb->GetCwndInSegments ();

	if (cubic_last_max <= segCwnd)
	{
		NS_LOG_DEBUG (std::to_string(id) + ">>>" + "lastMaxCwnd <= m_cWnd. K=0 and origin=" << segCwnd);
		cubic_k = 0.0;
		cubic_origin_point= segCwnd;
	}
	else
	{
		cubic_k = std::pow ((cubic_last_max- segCwnd) / bic_scale, 1 / 3.);
		cubic_origin_point =cubic_last_max;
		NS_LOG_DEBUG (std::to_string(id) + ">>>" +  "lastMaxCwnd > m_cWnd. K=" << cubic_k <<
				" and origin=" << cubic_last_max);
	}
	cnt = 0;
	lola_gradient = 100 * segCwnd;
}



void
TcpLoLa::DoCB(Ptr<TcpSocketState> tcb){
	NS_LOG_FUNCTION (std::to_string(id) << ">>>" <<this);
	if (lola_rtts->is_ready(tcb))
	{
		//TODO: Umbenennen der Variablen auf Berechnung in bytes
		//TODO: Target zu Zeitpunkt now + Measurement interval?
		int64_t target = std::pow((Simulator::Now() - cb_epoch_start + lola_rtts->get_current_RTT()).GetMilliSeconds()  ,3);
		target /= std::pow(lola_convergence_booster_sigma , 3);

		target = std::max(target, (int64_t)cb_Q_data_begin);
		int64_t q_data = ((int64_t)tcb->m_cWnd * (lola_rtts->get_current_RTT() - (lola_fixed_base_delay == Time(0) ? lola_curr_rtts->get_base()  : lola_fixed_base_delay)).GetMicroSeconds())/lola_rtts->get_current_RTT().GetMicroSeconds();

		NS_LOG_DEBUG (std::to_string(id) << ">>>" <<"Bytes " << q_data << " target " << target << " rtt " << lola_rtts->get_current_RTT() << " t " << (Simulator::Now() - cb_epoch_start).GetMilliSeconds() << " q_data_begin " << cb_Q_data_begin);

		if(cb_last_change + lola_curr_rtts->get_current_RTT() +(dynamic_cast<SimpleMin *>(lola_curr_rtts)->measurement_time) > Simulator::Now()){

			lola_gradient = 100*tcb->GetCwndInSegments(); //Gradient 1 MSS pro 100 RTTs (quasi gleichbleibendes cwnd)
			NS_LOG_DEBUG (std::to_string(id) << ">>>" << "doing stagnation (wait for measurement)");
		}
		else {if(q_data < target){

			//in contrast to kernel implementation - here no upper limit for increase

			lola_gradient = tcb->m_cWnd * (dynamic_cast<SimpleMin *>(lola_curr_rtts)->measurement_time).GetMicroSeconds() / // 10000 / //TODO: measurement time
					(((int32_t)target - (int32_t)q_data) * lola_curr_rtts->get_current_RTT().GetMicroSeconds());// lola_rtts->get_current_RTT().GetMicroSeconds());

			NS_LOG_DEBUG (std::to_string(id) << ">>>" << "doing normal calc - version " << lola_CB_version);
			//lola_gradient = std::max(lola_gradient, 2);
		}else{

			lola_gradient = 100*tcb->GetCwndInSegments(); //Gradient 1 MSS pro 100 RTTs (quasi gleichbleibendes cwnd)
			NS_LOG_DEBUG (std::to_string(id) << ">>>" << "doing stagnation");



		}

		cb_last_change = Simulator::Now();


		}//lola_rtts->next_measurement();
		if (!lola_gradient) lola_gradient = 1;
		NS_LOG_DEBUG (std::to_string(id) << ">>>" <<lola_gradient << " " << q_data );
	}
}




//add tcp-friendlyness

void
TcpLoLa::DoCUBIC(Ptr<TcpSocketState> tcb){
	NS_LOG_FUNCTION (std::to_string(id) << ">>>" <<this);
	Time t;
	uint32_t delta, bicTarget, cnt = 0;
	double offs;

	uint32_t segCwnd = tcb->GetCwndInSegments ();
	t = Simulator::Now () + lola_curr_rtts->get_current_RTT()- cubic_epoch_start;

	if (t.GetSeconds () < cubic_k)       /* t - K */
	{
		offs = cubic_k - t.GetSeconds ();
		NS_LOG_DEBUG (std::to_string(id) + ">>>" +  "t=" << t.GetSeconds () << " <k: offs=" << offs);
	}
	else
	{
		offs = t.GetSeconds () - cubic_k;
		NS_LOG_DEBUG (std::to_string(id) + ">>>" +  "t=" << t.GetSeconds () << " >= k: offs=" << offs);
	}


	/* Constant value taken from Experimental Evaluation of Cubic Tcp, available at
	 * eprints.nuim.ie/1716/1/Hamiltonpfldnet2007_cubic_final.pdf */
	delta = bic_scale * std::pow (offs, 3);

	NS_LOG_DEBUG (std::to_string(id) + ">>>" + "delta: " << delta);

	if (t.GetSeconds () < cubic_k)
	{
		// below origin
		bicTarget = cubic_origin_point- delta;
		NS_LOG_DEBUG (std::to_string(id) + ">>>" + "t < k: Bic Target: " << bicTarget);
	}
	else
	{
		// above origin
		bicTarget = cubic_origin_point+ delta;
		NS_LOG_DEBUG (std::to_string(id) + ">>>" + "t >= k: Bic Target: " << bicTarget);
	}

	// Next the window target is converted into a cnt or count value. CUBIC will
	// wait until enough new ACKs have arrived that a counter meets or exceeds
	// this cnt value. This is how the CUBIC implementation simulates growing
	// cwnd by values other than 1 segment size.
	if (bicTarget > segCwnd)
	{
		cnt = segCwnd / (bicTarget - segCwnd);
		NS_LOG_DEBUG (std::to_string(id) + ">>>" + "target>cwnd. cnt=" << cnt);
	}
	else
	{
		cnt = 100 * segCwnd;
	}

	// The maximum rate of cwnd increase CUBIC allows is 1 packet per
	// 2 packets ACKed, meaning cwnd grows at 1.5x per RTT.

	lola_gradient = std::max (cnt, 2U);
	NS_LOG_DEBUG (std::to_string(id) << ">>>" <<"gradient " << lola_gradient);
}

void
TcpLoLa::IncreaseByGradient(Ptr<TcpSocketState> tcb, uint32_t segmentsAcked){

	// apply Acks gracefully after gradient change (only change cwnd by 1 MSS from saved-up Acks)
	if (cnt > (uint32_t)std::abs(lola_gradient)){
		cnt = 0;
		tcb->m_cWnd += (lola_gradient > 0) ? (int32_t)tcb->m_segmentSize : (int32_t)-1 * tcb->m_segmentSize;
	}
	cnt += segmentsAcked;
	int32_t delta = (int32_t)cnt/lola_gradient;
	tcb->m_cWnd += delta * tcb->m_segmentSize;
	cnt -= delta * lola_gradient;
	//cWnd change
}


void
TcpLoLa::IncreaseWindow (Ptr<TcpSocketState> tcb, uint32_t segmentsAcked)
{
	NS_LOG_FUNCTION (std::to_string(id) << ">>>" <<this << tcb << segmentsAcked);



	if (in_hold && hi_end > Simulator::Now()){
		NS_LOG_DEBUG (std::to_string(id) << ">>>" << "In hold, measured RTT " << std::to_string(lola_curr_rtts->get_current_RTT().GetMicroSeconds()));
		return;
	}

	if (tcb->m_cWnd < tcb->m_ssThresh) lola_curr_rtts = ss_rtts; else lola_curr_rtts = lola_rtts;

	DoPreDecog(tcb);
	if (tcb->m_cWnd < tcb->m_ssThresh)
	{
		in_hold = in_convergence_booster = in_cubic = false;
		NS_LOG_DEBUG (std::to_string(id) << ">>>" << "Calling SS: "<< tcb->m_cWnd << ", " <<tcb->m_ssThresh);
		TcpNewReno::SlowStart(tcb, segmentsAcked);
	}
	else //if ( lola_last_recalc + lola_min_target_recalc_interval <= Simulator::Now() )
	{
		NS_LOG_DEBUG (std::to_string(id) << ">>>" << "No SS: "<< tcb->m_cWnd << ", " <<tcb->m_ssThresh);
		NS_LOG_DEBUG (std::to_string(id) << ">>>" << "Flags: " << in_cubic << " " << in_convergence_booster << " " << in_hold);
		if (!(in_cubic || in_convergence_booster || in_hold)) StartCUBIC(tcb);
		if (in_cubic) DoCUBIC(tcb);
		//if (in_convergence_booster) DoCB(tcb);
		if (in_convergence_booster && lola_CB_version == 1) DoCB(tcb);

		if (! in_hold) IncreaseByGradient(tcb, segmentsAcked);

	}
	//tcb->m_cWnd = tcb->m_segmentSize * 50;
	NS_LOG_DEBUG (std::to_string(id) + ">>>" + "IncreaseWindow-- " << tcb->GetCwndInSegments() << " " << lola_curr_rtts->get_current_RTT().GetMicroSeconds() << " " << (lola_fixed_base_delay == Time(0) ? lola_curr_rtts->get_base()  : lola_fixed_base_delay).GetMicroSeconds() << " Flags: " << in_cubic << " " << in_convergence_booster << " " << in_hold);
	if (! in_hold && lola_curr_rtts->is_ready(tcb)){

		lola_curr_rtts->next_measurement();
	}

}

std::string
TcpLoLa::GetName () const
{
	return "TcpLoLa";
}

uint32_t
TcpLoLa::GetSsThresh (Ptr<const TcpSocketState> tcb,
		uint32_t bytesInFlight)
{
	NS_LOG_FUNCTION (std::to_string(id) << ">>>" <<this << tcb << bytesInFlight);
	in_hold = in_convergence_booster = in_cubic = false;
	cubic_last_max = tcb->GetCwndInSegments();
	lola_rtts->lock(lola_curr_rtts->get_current_RTT());



	//return std::max (std::min (tcb->m_ssThresh.Get (), tcb->m_cWnd.Get () - tcb->m_segmentSize), 2 * tcb->m_segmentSize);
	return std::max ((tcb->m_cWnd.Get() * 717 ) << 10, 2 * tcb->m_segmentSize);
}

} // namespace ns3
