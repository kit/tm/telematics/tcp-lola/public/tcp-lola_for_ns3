/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2018,
 *     Karlsruhe Institute of Technology
 *     Institute of Telematics
 *     Zirkel 2, 76131 Karlsruhe
 *     Germany
 *
 *     Author: Felix Neumeister <felix.neumeister@student.kit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef TCPLOLA_H
#define TCPLOLA_H

#include "ns3/tcp-congestion-ops.h"

namespace ns3 {

class RTTAccounting : public Object{
public:
	static TypeId GetTypeId (void);
	virtual std::string GetName () const{
		return "RTTAccounting";
	}
	virtual void update_RTT(Ptr<TcpSocketState> tcb, uint32_t segmentsAcked,
			const Time& rtt){};

	virtual bool is_ready(Ptr<TcpSocketState> tcb) {
		return true;
	}
	virtual ~RTTAccounting(){}
	virtual Time get_current_RTT() =0;
	virtual void next_measurement(){}
	virtual Time get_base() =0;
	virtual void reset_all() {}
	virtual void lock(Time unlock) {}
	virtual void set_hold(bool hold){}
	virtual void set_fixed_base(Time base){}
};

class SimpleMin : public RTTAccounting{
public:
	SimpleMin();
	SimpleMin(Time measurement_time);

	void update_RTT(Ptr<TcpSocketState> tcb, uint32_t segmentsAcked,
			const Time& rtt);
	bool is_ready(Ptr<TcpSocketState> tcb) ;
	Time get_current_RTT();
	void next_measurement();
	Time get_base() ;
	void reset_all() ;
	void lock(Time unlock);
	Time measurement_time;
	void set_hold(bool hold);
	void set_fixed_base(Time base);
protected:
	Time rtt_min;
	Time rtt_base;

	Time measurement_end;
	uint32_t measurement_count;
	Time locked_until;
	bool in_hold;
	Time fixed_base;

};
class SSMin : public SimpleMin{
public:

	bool is_ready(Ptr<TcpSocketState> tcb) ;
	void next_measurement();

};
class MaxofMin : public SimpleMin{
public:
	MaxofMin();
	MaxofMin(Time time);
	void update_RTT(Ptr<TcpSocketState> tcb, uint32_t segmentsAcked,
			const Time& rtt);
	bool is_ready(Ptr<TcpSocketState> tcb) ;
	Time get_current_RTT();
	void set_hold(bool hold);

private:
	Time holdMax;
};


/**
 * \ingroup congestionOps
 *
 * \brief An implementation of TCP LoLa
 *
 * TCP LoLa is a delay-based congestion control algorithm described in http://dx.doi.org/10.1109/49.464716.
 */

class TcpLoLa : public TcpNewReno
{
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);

  /**
   * Create an unbound tcp socket.
   */
  TcpLoLa (void);

  /**
   * \brief Copy constructor
   * \param sock the object to copy
   */
  TcpLoLa (const TcpLoLa& sock);
  virtual ~TcpLoLa (void);
  TcpLoLa copy();

  virtual std::string GetName () const;

  /**
   * \brief Compute RTTs needed to execute LoLa algorithm
   *
   *
   * \param tcb internal congestion state
   * \param segmentsAcked count of segments ACKed
   * \param rtt last RTT
   *
   */
  virtual void PktsAcked (Ptr<TcpSocketState> tcb, uint32_t segmentsAcked,
                          const Time& rtt);

  /**
   * \brief Enable/disable CC algorithm
   *
   * \param tcb internal congestion state
   * \param newState new congestion state to which the TCP is going to switch
   */
  virtual void CongestionStateSet (Ptr<TcpSocketState> tcb,
                                   const TcpSocketState::TcpCongState_t newState);

  /**
   * \brief Main function determining the behavior of TCP LoLa
   *
   * \param tcb internal congestion state
   * \param segmentsAcked count of segments ACKed
   */
  virtual void IncreaseWindow (Ptr<TcpSocketState> tcb, uint32_t segmentsAcked);

  /**
   * \brief Get slow start threshold
   *
   * \param tcb internal congestion state
   * \param bytesInFlight bytes in flight
   *
   * \return the slow start threshold value
   */
  virtual uint32_t GetSsThresh (Ptr<const TcpSocketState> tcb,
                                uint32_t bytesInFlight);

  virtual Ptr<TcpCongestionOps> Fork ();
  virtual void set_fixed_base_delay(Time base);

protected:
private:


  void DoPreDecog(Ptr<TcpSocketState> tcb);
  void StartCB(Ptr<TcpSocketState> tcb) ;
  void StartCUBIC(Ptr<TcpSocketState> tcb);

  void DoCUBIC(Ptr<TcpSocketState> tcb);
  void DoCB(Ptr<TcpSocketState> tcb);
  void IncreaseByGradient(Ptr<TcpSocketState> tcb, uint32_t segmentsAcked);
  int32_t enhance(int64_t cwnd);
  int32_t smooth(int64_t cwnd);

private:

 // LoLa general parameters
  int id = 0;

 uint32_t beta = 717;
 uint32_t initial_ssthresh = 2;
 double bic_scale = 4.1;
 uint32_t scale = 4;


 Time lola_queue_target;
 uint32_t lola_gamma;
 uint32_t lola_CWnd_min;
 Time lola_convergence_booster_start_delay ;
 uint32_t lola_convergence_booster_sigma ;
 uint32_t lola_CB_version;
 Time lola_hold_time ;
 Time lola_min_target_recalc_interval ;
 
 

 //LoLa mode
 bool lola_do_precautionary_decongestion;
 bool lola_do_convergence_booster ;
 bool lola_do_CWnd_hold ;
 Time lola_last_recalc ;
 int32_t lola_gradient;

 // Connection specific variables
 // CUBIC Curve
 Time cubic_epoch_start;
 uint32_t cubic_origin_point;
 double cubic_k;
 uint32_t cubic_last_max;
 uint32_t cnt;

 
 // Convergence Booster state
 Time cb_epoch_start;
 int32_t cb_Q_data_begin;
 uint32_t cb_gradient_version;
 Time cb_last_change;


 // Hold interval
 Time hi_end;
 // Loss Recovery
 uint32_t lr_loss_cwnd;

 // LoLa state

  bool in_hold ;
  bool in_convergence_booster ;
  bool in_cubic ;

 // Base RTT Measurement
  int lola_rtt_filter;
  Time lola_fixed_base_delay;
  RTTAccounting *lola_rtts;
  RTTAccounting *ss_rtts;
  RTTAccounting *lola_curr_rtts;


};


} // namespace ns3

#endif // TCPLOLA_H
