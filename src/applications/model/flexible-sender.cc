/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2018,
 *     Karlsruhe Institute of Technology
 *     Institute of Telematics
 *     Zirkel 2, 76131 Karlsruhe
 *     Germany
 *
 *     Author: Felix Neumeister <felix.neumeister@student.kit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * This program is in large parts based on the TCP bulk send application,
 * shipped with ns-3 Copyright (c) 2010 Georgia Institute of Technology
 * (Author: George F. Riley <riley@ece.gatech.edu>). However
 * it was amended with some more attributes to allow to customize the underlying
 * Internet stack to the needs of testing congestion control algorithms with per-flow parameters.
 */


#include "ns3/log.h"
#include "ns3/address.h"
#include "ns3/node.h"
#include "ns3/nstime.h"
#include "ns3/socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/pointer.h"
#include "ns3/uinteger.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/tcp-socket-factory.h"
#include "flexible-sender.h"
#include "ns3/tcp-socket-base.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("FlexibleSender");

NS_OBJECT_ENSURE_REGISTERED (FlexibleSender);

TypeId
FlexibleSender::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::FlexibleSender")
    .SetParent<Application> ()
    .SetGroupName("Applications") 
    .AddConstructor<FlexibleSender> ()
	.AddAttribute ("InitialWindow", "The initial cwnd",
	                   UintegerValue (0),
	                   MakeUintegerAccessor (&FlexibleSender::m_initialWindow),
	                   MakeUintegerChecker<uint32_t> (1))
   .AddAttribute ("InitialSSThresh", "The initial SSthresh",
					   UintegerValue (65000),
					   MakeUintegerAccessor (&FlexibleSender::m_initialSSthresh),
					   MakeUintegerChecker<uint32_t> (1))
	.AddAttribute ("SendSize", "The amount of data to send each time.",
				  UintegerValue (1500),
				  MakeUintegerAccessor (&FlexibleSender::m_sendSize),
				  MakeUintegerChecker<uint32_t> (1))
	.AddAttribute ("Remote", "The address of the destination",
				  AddressValue (),
				  MakeAddressAccessor (&FlexibleSender::m_peer),
				  MakeAddressChecker ())
    //.AddAttribute ("SocketFactory", "The address of the destination",
    //               AddressValue (),
    //               MakeAddressAccessor (&FlexibleSender::m_peer),
    //               MakeAddressChecker ())
    .AddAttribute ("MaxBytes",
                   "The total number of bytes to send. "
                   "Once these bytes are sent, "
                   "no data  is sent again. The value zero means "
                   "that there is no limit.",
                   UintegerValue (0),
                   MakeUintegerAccessor (&FlexibleSender::m_maxBytes),
                   MakeUintegerChecker<uint64_t> ())
    .AddAttribute ("Protocol", "The type of protocol to use.",
                   TypeIdValue (TcpSocketFactory::GetTypeId ()),
                   MakeTypeIdAccessor (&FlexibleSender::m_tid),
                   MakeTypeIdChecker ())
	.AddAttribute ("SmallBuffer", "Keep buffer filling small to save RAM.",
				   BooleanValue(true),
				   MakeBooleanAccessor(&FlexibleSender::m_smallQueue),
				   MakeTypeIdChecker ())
	.AddAttribute ("CC", "The type of protocol to use.",
				   ObjectFactoryValue (ObjectFactory("ns3::TcpLoLa")),
				   MakeObjectFactoryAccessor(&FlexibleSender::m_cc),
				   MakeObjectFactoryChecker())
	.AddAttribute ("socket", "The type of protocol to use.",
				   PointerValue (NULL),
				   MakePointerAccessor(&FlexibleSender::m_socket),
				   MakePointerChecker<TcpSocket>())
	.AddTraceSource ("Tx", "A new packet is created and is sent",
                   MakeTraceSourceAccessor (&FlexibleSender::m_txTrace),
                  "ns3::Packet::TracedCallback")

  ;
  return tid;
}


FlexibleSender::FlexibleSender ()
  : m_socket (0),
    m_connected (false),
	m_sendSize (1500),
	m_maxBytes (0),
    m_totBytes (0),
	m_tid (),
	m_smallQueue (true),

	m_cc(),
	m_initialWindow (0),
	m_initialSSthresh (0)




{
  NS_LOG_FUNCTION (this);
}

FlexibleSender::~FlexibleSender ()
{
  NS_LOG_FUNCTION (this);
}

void
FlexibleSender::SetMaxBytes (uint64_t maxBytes)
{
  NS_LOG_FUNCTION (this << maxBytes);
  m_maxBytes = maxBytes;
}

Ptr<Socket>
FlexibleSender::GetSocket (void) const
{
  NS_LOG_FUNCTION (this);
  return m_socket;
}

void
FlexibleSender::DoDispose (void)
{
  NS_LOG_FUNCTION (this);

  m_socket = 0;
  // chain up
  Application::DoDispose ();
}

// Application Methods
void FlexibleSender::StartApplication (void) // Called at time specified by Start
{
  NS_LOG_FUNCTION (this);

  // Create the socket if not already
  if (!m_socket)
    {
      m_socket = Socket::CreateSocket (GetNode (), m_tid);

      // Fatal error if socket type is not NS3_SOCK_STREAM or NS3_SOCK_SEQPACKET
      if (m_socket->GetSocketType () != Socket::NS3_SOCK_STREAM &&
          m_socket->GetSocketType () != Socket::NS3_SOCK_SEQPACKET)
        {
          NS_FATAL_ERROR ("Using BulkSend with an incompatible socket type. "
                          "BulkSend requires SOCK_STREAM or SOCK_SEQPACKET. "
                          "In other words, use TCP instead of UDP.");
        }

      if (Inet6SocketAddress::IsMatchingType (m_peer))
        {
          if (m_socket->Bind6 () == -1)
            {
              NS_FATAL_ERROR ("Failed to bind socket");
            }
        }
      else if (InetSocketAddress::IsMatchingType (m_peer))
        {
          if (m_socket->Bind () == -1)
            {
              NS_FATAL_ERROR ("Failed to bind socket");
            }
        }


      //------------Configute Connection-------------------------

      Ptr<TcpCongestionOps> newcc = m_cc.Create<TcpCongestionOps>();
      m_socket->GetObject<TcpSocketBase>()->SetCongestionControlAlgorithm(newcc);
      if (m_initialWindow != 0)
    	  m_socket->GetObject<TcpSocketBase>()->SetAttribute("InitialCwnd", UintegerValue(m_initialWindow));
      if (m_initialSSthresh)
    	  m_socket->GetObject<TcpSocket>()->SetAttribute("InitialSlowStartThreshold", UintegerValue(m_initialSSthresh));

      //------------Start Tracing------------------------------






      //-------------------------------------------------------


      m_socket->Connect (m_peer);
      m_socket->ShutdownRecv ();
      m_socket->SetConnectCallback (
        MakeCallback (&FlexibleSender::ConnectionSucceeded, this),
        MakeCallback (&FlexibleSender::ConnectionFailed, this));
      //m_socket->SetSendCallback (
      //  MakeCallback (&FlexibleSender::DataSend, this));
      m_socket->SetDataSentCallback (
              MakeCallback (&FlexibleSender::DataSend, this));
    }
  if (m_connected)
    {
      SendData ();
    }
}

void FlexibleSender::StopApplication (void) // Called at time specified by Stop
{
  NS_LOG_FUNCTION (this);

  if (m_socket != 0)
    {
      m_socket->Close ();
      m_connected = false;
    }
  else
    {
      NS_LOG_WARN ("BulkSendApplication found null socket to close in StopApplication");
    }
}


// Private helpers

void FlexibleSender::SendData (void)
{
	NS_LOG_FUNCTION (this);
	  Ptr<TcpSocketBase> sock;
	    sock = m_socket->GetObject<TcpSocketBase>();

	  while (m_maxBytes == 0 || m_totBytes < m_maxBytes)
	    { // Time to send more

	      // uint64_t to allow the comparison later.
	      // the result is in a uint32_t range anyway, because
	      // m_sendSize is uint32_t.
	      uint64_t toSend = m_sendSize;
	      // Make sure we don't send too many
	      if (m_maxBytes > 0)
	        {
	          toSend = std::min (toSend, m_maxBytes - m_totBytes);
	        }

	      if (sock->BufferSaturated()){
	         	  break;
	           }

	      NS_LOG_LOGIC ("sending packet at " << Simulator::Now ());
	      Ptr<Packet> packet = Create<Packet> (toSend);
	      int actual = m_socket->Send (packet);
	      if (actual > 0)
	        {
	          m_totBytes += actual;
	          m_txTrace (packet);
	        }
	      // We exit this loop when actual < toSend as the send side
	      // buffer is full. The "DataSent" callback will pop when
	      // some buffer space has freed ip.
	      if ((unsigned)actual != toSend)
	        {
	          break;
	        }
	    }
	  // Check if time to close (all sent)
	  if (m_totBytes == m_maxBytes && m_connected)
	    {
	      m_socket->Close ();
	      m_connected = false;
	    }
	}



void FlexibleSender::ConnectionSucceeded (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this << socket);
  NS_LOG_LOGIC ("FlexibleSender Connection succeeded");
  m_connected = true;
  SendData ();
}

void FlexibleSender::ConnectionFailed (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this << socket);
  NS_LOG_LOGIC ("FlexibleSender, Connection Failed");
}

void FlexibleSender::DataSend (Ptr<Socket>, uint32_t)
{
  NS_LOG_FUNCTION (this);

  if (m_connected)
    { // Only send new data if the connection has completed
      SendData ();
    }
}



} // Namespace ns3
